# Zebringo

Ce dépôt contient le code source du site « Zèbringo, une aventure dont vous êtes l'héroïne ou le héros ». Il utilise le framework Hugo pour la génération des pages HTML, et le template bearblog pour l'agencement des pages.

Pour jouer à Zèbringo, rendez-vous sur [le mini-site dédié](https://isabelle-santos.space/zebringo).

## License

Cette aventure textuelle est distribuée sous la license CC-BY-SA 4.0. Ça veut dire que vous pouvez la ré-utiliser et la redistribuer comme vous le souhaitez, à condition d'indiquer d'où ça vient et de repartager avec une license semblable.

**Si vous utilisez Zèbringo, faites moi signe, ça me fera super plaisir !** Vous êtes pas obligé, hein. Mais un chiot sera triste si vous le faites pas.

## Contribuer

Vous avez une super idée pour améliorer Zèbringo ? Vous avez repéré une erreur ? Vous pouvez venir m'en parler sur [Mastodon](https://framapiaf.org/@Moutmout), ou bien créer un fork et faire une merge request, ou bien créer un ticket, ou bien m'envoyer un pigeon voyageur. 
