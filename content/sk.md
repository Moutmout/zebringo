---
title: "Score Keeper"
date: "2023-03-28"
draft: false
---

Bravo ! Tu as gagné le poste de *Score Keeper* (SK) ! Il y a deux SK qui sont chacun·e en binôme avec un·e JR. 

Avant le début du match, discute avec ton ou ta JR pour décider des signes que vous allez utiliser pour communiquer. Pendant le match il faut :

- Communiquer avec ton ou ta JR.
- Noter les points à chaque passage marquant.
- Calculer le score total.
- Communiquer avec lae SBO.
- Noter la prise de lead et les star pass.
- Noter la raison pour laquelle chaque jam se termine.

[Si tu te dis que ce poste te ferait envie si seulement il y avait moins de calculs à faire, clique ici.](../sbo/index.html)


![Mr. Bean qui copie sur son voisin. Le texte autour dit « quand t'es SK et que t'as pas le même total de points que lae SBO».](../img/SK-SBO-score.png)
