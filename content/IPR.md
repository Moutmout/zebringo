---
title: "Tu reconnais un pack les yeux fermés "
date: "2023-03-28"
draft: false
---

Bravo ! Tu as gagné le poste d’Inner Pack Ref (IPR) ! 

Si tu es IPR-front, tu auras une vue globale sur le jeu et tu devras : 
- Rester devant la personne la plus à l’avant du pack. 
- Dire si le lead est ouvert ou fermé. 
- Surveiller les forearms, les multiplayers, les blockers qui cuttent, les illegal positions,… 

Si tu es IPR-back, tu devras : 
- Annoncer « No Pack » et « Pack is here ». 
- Surveiller les out-of-play à l’arrière du pack 
- Communiquer avec l’IPR-front quand le pack passe vers l’avant : « Pack is front ! ».

![Photo d'un arbitre qui montre où est le pack]()
