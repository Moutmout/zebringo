---
title: "1, 2, 3, 4"
date: "2023-03-28"
draft: false
---

Bravo ! Tu as gagné le poste de Jammer Ref (JR) ! Tu n’as qu’une seule mission : surveiller tout ce qui peut arriver à ton ou ta jammeureuse. Mais c’est pas pour autant que ce sera facile. 

Tes responsabilités : 

- Connaître l’état du lead. 
- Voir si ton ou ta jammeureuse cut ou fait un backblock. 
- Compter les points pendant chaque passage marquant. 
- Valider (ou non) les star pass. 
- Communiquer avec les SK. 

Tu dois rester à côté du ou de la jammeureuse que tu suis, mais pas besoin de patiner super vite : tu peux couper par le centre lorsqu’iel fait le tour du track. 

![Une jammer ref qui montre lae jammeureuse qui a le lead.](../img/jr.jpg)
