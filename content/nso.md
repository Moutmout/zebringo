---
title: "Là, tout de suite, t'as pas envie de patiner"
date: "2023-03-28"
draft: false
---

Ou peut-être que tu kiffes juste les chronos, les stats ou les tableurs. Dans tous les cas, il y a un poste de Non-Skating Official (NSO) qui t’attend. 

On appelle parfois les Non Skating Officials des zèbres, à cause de leur maillot à rayures. Sur certains événements, les Non Skating Officials ont un t-shirt rose, aux couleurs de la WFTDA. C’est pourquoi on les appelle parfois les flamingos, ou flamants roses.  Ensemble, iels forment la team zèbringo ! 

![Un dessin de flamant rose](../img/flamingo.png)

- [Tu sais te montrer pédagogue quand il s'agit des règles du derby.](../pbm/index.html)
- [Tu aimerais mieux connaître les conditions à remplir pour prendre le lead ou pour marquer des points.](../sk/index.html)
- [Tu ne sais pas (encore) dans quel cas deux jammeureuses peuvent être en PB simultanément.](../nso_non_pbm/index.html)
- [Tu veux pas patiner, mais t'as quand même envie de te dépenser physiquement.](../track_ninja/index.html)
