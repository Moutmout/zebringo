---
title: "T'as même pas peur des chronos"
date: "2023-03-29"
draft: false
---

Bravo ! Tu as gagné le poste de Jam Timer (JT) ! Avec tes deux chronos et ton sifflet, c’est toi qui donne le top départ aux joueureuses. 

Tu devras :

- Gérer les chronos de période et de jam.
- Vérifier le chrono affiché sur le scoreboard.
- Siffler la fin des jams au bout de 2 minutes.
- Siffler les arrêts et les reprises de jeu.
- Indiquer la raison de l’arrêt de jeu (Team Timeout, Official Review ou Official Timeout) avec tes bras.

![Photo d'un jam timer qui indique un Official Time Out.](../img/OTO.jpg)
![Photo d'une jam timer qui indique un Official Review.](../img/OR.jpg)

[Si gérer des chronos ne te suffit pas, tu veux gérer *le* chrono le plus important du match ! Et puis, ça te fait un peu peur de te faire foncer dedans par quelqu'un en patins, clique ici.](../sbo/index.html)
