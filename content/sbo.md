---
title: "Tu aimes les ordinateurs"
date: "2023-03-29"
draft: false
---

Bravo ! Tu as gagné le poste de Scoreboard Operator (SBO) ! Tes fonctions :

- Noter le score dans le logiciel.
- Communiquer avec les SK et veiller à l’exactitude du score affiché.
- Noter les départs de jam et les arrêts de jeu.
- Communiquer avec lae JT pour éviter les erreurs de chrono.
- Renseigner les jammeureuses et pivot au début de chaque jam.
- Afficher qui a le lead.

Si le cœur t’en dit, tu peux aussi utiliser les données générées par le logiciel de scoreboard pour faire des stats. Il y a des scripts Python qui permettent de tout automatiser !
