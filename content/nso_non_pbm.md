---
title: "NSO, oui ! Savoir chronométrer les jammeureuses, bof."
date: "2023-03-28"
draft: false
---

Si tu hésite à sauter directement dans le grand bain, tu peux aussi être « ghost » sur un ou deux matchs. Tu suivra alors un·e officiel·le qui t’expliquera ce qu’iel fait et te donnera plein de petits tips. 

En tant que ghost, tu peux même observer un·e officiel·le plus expérimenté·e pendant la première période, puis pendant la deuxième période arbitrer toi-même, avec cet·te officiel·le qui t’aide si besoin. 

![Dessin d'un fantome](../img/ghost.png)

- [Tu es à l'aise avec un ordinateur](../sbo/index.html)
- [Les chronos te font pas peur](../jt/index.html)
- [Tu aimerais mieux connaître les différents types de fautes](../pbt/index.html)
- [Tu veux être au cœur de l'action](../plt/index.html)
