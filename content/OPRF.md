---
title: "Tu aimes patiner à l'envers"
date: "2023-03-28"
draft: false
---

Bravo ! Tu as gagné le poste d’Outer Pack Ref Front (OPR-F) ! Tu vas pouvoir développer ta vitesse et parfaire tes transitions. En effet, tu dois rester à l’avant du pack tout en ayant un œil sur l’action. Et étant à l’extérieur du track, tu as plus de chemin à parcourir que les joueureuses. 

## Tes responsabilités : 
- Rester devant la personne la plus en avant du pack. 
- Surveiller les multiplayers à l’avant du pack. 
- Surveiller les cuts à l’extérieur du track. 

![Photo d'un arbitre qui patine à l'envers](.../img/OPRF.jpg)

N'hésite pas à tester d'autres postes aussi, y compris des postes de NSO !
[Clique ici pour revenir au début](../index.html)
