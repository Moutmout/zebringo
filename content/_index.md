(↑ À tout moment, vous pourrez utiliser ce bouton pour revenir sur cette page.)

Une aventure minimaliste dont **vous** êtes l'héroïne ou le héros.

[Commencer](debut_aventure/index.html)

Ce mini-site est à destination des personnes déjà familières du derby et qui souhaiteraient se lancer dans l'arbitrage, mais qui ne savent pas quel poste choisir. Il est fourni dans l'espoir qu'il puisse être utile ou divertissant, mais sans aucune garantie de l'exactitude des informations publiées.
