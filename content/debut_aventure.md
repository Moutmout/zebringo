---
title: "Le début de l'aventure"
date: "2023-03-28"
draft: false
---


Tu aimerais bien mieux connaitre les règles, améliorer tes techniques de patinage, comprendre pourquoi tu te retrouves en box,… Alors enfile un maillot à rayures (ou un haut noir, cœur sur les NSO) ! 

Je t’entends déjà dire «  Mais je ne connais pas assez bien les règles !  ». Peut-être que justement, tu as besoin de mettre les mains dans le cambouis pour pouvoir les apprendre ! 

L’arbitrage te donnera aussi quelque chose à faire les jours où ton corps te dit «  non, aujourd’hui tu joues pas  ». Et même si tu ne joues pas (ou plus) au derby, l’arbitrage est un hobby amusant par lui-même. Enfin, aux tournois, les buffets pour les refs et NSO sont toujours au top. 

----

Face à ces arguments imparables, tu as décidé de rejoinder le crew d’officiel·les du prochain scrimmage. Mais  tu ne sais pas quel poste choisir. Ce petit site va t’aider. 

Tu te souviens des « livre dont vous êtes le héros » de ton enfance ? Là c’est pareil : choisi la suite de ton histoire en cliquant sur les liens.

- [Si tu es à l'aise sur des patins, clique ici.](../so/index.html)
- [Si aujourd'hui, t'as pas envie de patiner, clique ici.](../nso/index.html)
